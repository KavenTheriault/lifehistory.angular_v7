import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ApiService } from '../_services/api.service';

@Component({
  selector: 'app-activity-type-edit',
  templateUrl: './activity-type-edit.component.html',
  styleUrls: ['./activity-type-edit.component.css']
})
export class ActivityTypeEditComponent implements OnInit {

  activity_type_form: FormGroup;
  activity_type = null;
  submitted = false;
  loading = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getActivityType(this.route.snapshot.params['id']);
    this.activity_type_form = this.formBuilder.group({
      name: ['', Validators.required],
      show_quantity: [false, Validators.required],
      show_rating: [false, Validators.required],
    });
  }

  getActivityType(id) {
    this.api.getActivityType(id).subscribe(data => {
      this.activity_type = data;
      this.activity_type_form.setValue({
        name: data.name,
        show_quantity: data.show_quantity,
        show_rating: data.show_rating
      });
    });
  }

  get f() { return this.activity_type_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.activity_type_form.invalid) {
      return;
    }

    this.loading = true;

    this.activity_type.name = this.f.name.value;
    this.activity_type.show_quantity = this.f.show_quantity.value;
    this.activity_type.show_rating = this.f.show_rating.value;

    this.api.updateActivityType(this.activity_type.id, this.activity_type)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        this.router.navigate(['categories']);
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  back() {
    this.router.navigate(['categories']);
  }

}
