import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeEntryEditComponent } from './life-entry-edit.component';

describe('LifeEntryEditComponent', () => {
  let component: LifeEntryEditComponent;
  let fixture: ComponentFixture<LifeEntryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeEntryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeEntryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
