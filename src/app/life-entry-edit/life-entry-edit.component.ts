import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbTime } from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';

import { ApiService } from '../_services/api.service';
import { LifeEntry } from '../_models/life-entry';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time-struct';
import { LifeEntryActivity } from '../_models/life-entry-activity';

@Component({
  selector: 'app-life-entry-edit',
  templateUrl: './life-entry-edit.component.html',
  styleUrls: ['./life-entry-edit.component.css']
})
export class LifeEntryEditComponent implements OnInit {

  life_entry: LifeEntry;
  life_entry_form: FormGroup;
  submitted = false;
  loading = false;
  save_and_add = false;
  life_entry_activity_to_delete: LifeEntryActivity;
  another = false;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getLifeEntry(this.route.snapshot.params['id']);
    this.life_entry_form = this.formBuilder.group({
      start_time: [null, Validators.required],
      end_time: [null],
    });
  }

  getLifeEntry(id) {
    this.api.getLifeEntry(id).subscribe(data => {
      this.life_entry = data;
      this.life_entry_form.setValue({
        start_time: this.stringToTime(data.start_time),
        end_time: data.end_time ? this.stringToTime(data.end_time) : null,
      });
    });
  }

  get f() { return this.life_entry_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.life_entry_form.invalid) {
      return;
    }

    this.loading = true;

    this.life_entry.start_time = this.timeToString(this.f.start_time.value);
    this.life_entry.end_time = this.f.end_time.value ? this.timeToString(this.f.end_time.value) : null;

    this.api.updateLifeEntry(this.life_entry.id, this.life_entry)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        if (this.another)
          this.router.navigate(['life_entries/new'], { queryParams: { dayId: this.life_entry.day_id } });
        else if (this.save_and_add)
          this.router.navigate(['life_entry_activities/new'], { queryParams: { lifeEntryId: this.life_entry.id } });
        else
          this.back();
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  setLifeEntryActivityToDelete(life_entry_activity: LifeEntryActivity) {
    this.life_entry_activity_to_delete = life_entry_activity;
  }

  deleteLifeEntryActivity() {
    this.loading = true;
    this.api.deleteLifeEntryActivity(this.life_entry_activity_to_delete.id)
      .subscribe(res => {
        this.loading = false;
        let index = this.life_entry.life_entry_activities.indexOf(this.life_entry_activity_to_delete);
        this.life_entry.life_entry_activities.splice(index, 1);
      }, (err) => {
        console.log(err);
        this.loading = false;
      }
      );
  }

  timeToString(time: NgbTime) {
    return `${time.hour}:${time.minute}:${time.second}`;
  }

  stringToTime(timeString: string): NgbTimeStruct {
    let parts = timeString.split(':', 3);
    return { hour: +parts[0], minute: +parts[1], second: +parts[2] }
  }

  editLifeEntryActivity(id: any) {
    this.router.navigate([`life_entry_activities/${id}`]);
  }

  back() {
    this.router.navigate([`days/${this.life_entry.day_id}`]);
  }
}
