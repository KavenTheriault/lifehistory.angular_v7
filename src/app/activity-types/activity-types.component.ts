import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../_services/api.service';
import { ActivityType } from '../_models/activity-type';

@Component({
  selector: 'app-activity-types',
  templateUrl: './activity-types.component.html',
  styleUrls: ['./activity-types.component.css']
})
export class ActivityTypesComponent implements OnInit {

  activity_types: ActivityType[] = [];
  activity_type_to_delete: ActivityType;
  loading = true;
  curPage: number = 1;
  pageSize: number = 10;

  constructor(
    private router: Router,
    private api: ApiService) { }

  ngOnInit() {
    this.api.getActivityTypes()
      .subscribe(result => {
        this.activity_types = result.sort((a, b) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        });
        console.log(this.activity_types);
        this.loading = false;
      }, err => {
        console.log(err);
        this.loading = false;
      });
  }

  setActivityTypeToDelete(activity_type: ActivityType) {
    this.activity_type_to_delete = activity_type;
  }

  deleteActivityType() {
    this.loading = true;
    this.api.deleteActivityType(this.activity_type_to_delete.id)
      .subscribe(res => {
        this.loading = false;
        let index = this.activity_types.indexOf(this.activity_type_to_delete);
        this.activity_types.splice(index, 1);
      }, (err) => {
        console.log(err);
        this.loading = false;
      }
      );
  }

  newActivityType() {
    this.router.navigate(['categories/new']);
  }

  editActivityType(id: any) {
    this.router.navigate([`categories/${id}`]);
  }

}
