import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeEntryActivityEditComponent } from './life-entry-activity-edit.component';

describe('LifeEntryActivityEditComponent', () => {
  let component: LifeEntryActivityEditComponent;
  let fixture: ComponentFixture<LifeEntryActivityEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeEntryActivityEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeEntryActivityEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
