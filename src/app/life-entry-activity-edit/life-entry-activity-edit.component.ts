import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap, catchError } from 'rxjs/operators';

import { ApiService } from '../_services/api.service';
import { LifeEntryActivity } from '../_models/life-entry-activity';
import { Activity } from '../_models/activity';

@Component({
  selector: 'app-life-entry-activity-edit',
  templateUrl: './life-entry-activity-edit.component.html',
  styleUrls: ['./life-entry-activity-edit.component.css']
})
export class LifeEntryActivityEditComponent implements OnInit {

  life_entry_activity: LifeEntryActivity;
  life_entry_activity_form: FormGroup;
  submitted = false;
  loading = false;
  searching = false;
  searchFailed = false;
  another = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getLifeEntryActivity(this.route.snapshot.params['id']);
    this.life_entry_activity_form = this.formBuilder.group({
      activity: [null, Validators.required],
      description: [''],
      quantity: [1],
      rating: [0],
    });
  }

  getLifeEntryActivity(id) {
    this.api.getLifeEntryActivity(id).subscribe(data => {
      this.life_entry_activity = data;
      this.life_entry_activity_form.setValue({
        activity: data.activity,
        description: data.description,
        quantity: data.quantity,
        rating: data.rating,
      });
    });
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      tap(() => this.searching = true),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.api.searchActivities(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          })
        ),
      ),
      tap(() => this.searching = false),
    )

  formatter = (x: Activity) => x.name;

  get f() { return this.life_entry_activity_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.life_entry_activity_form.invalid) {
      return;
    }

    this.loading = true;

    this.life_entry_activity.activity_id = this.f.activity.value.id;
    this.life_entry_activity.description = this.f.description.value;
    this.life_entry_activity.quantity = this.f.quantity.value;
    this.life_entry_activity.rating = this.f.rating.value;

    this.api.updateLifeEntryActivity(this.life_entry_activity.id, this.life_entry_activity)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        if (this.another)
          this.router.navigate(['life_entry_activities/new'], { queryParams: { lifeEntryId: this.life_entry_activity.life_entry_id } });
        else
          this.back();
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  back() {
    this.router.navigate([`life_entries/${this.life_entry_activity.life_entry_id}`]);
  }

}
