import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { AppRoutingModule } from './app-routing.module';
import { SignupComponent } from './signup/signup.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ActivityTypesComponent } from './activity-types/activity-types.component';
import { ActivityTypeAddComponent } from './activity-type-add/activity-type-add.component';
import { ActivityTypeEditComponent } from './activity-type-edit/activity-type-edit.component';
import { AuthInterceptor } from './_interceptors/auth-interceptor';
import { ActivitiesComponent } from './activities/activities.component';
import { ActivityAddComponent } from './activity-add/activity-add.component';
import { ActivityEditComponent } from './activity-edit/activity-edit.component';
import { DayEditComponent } from './day-edit/day-edit.component';
import { LifeEntryAddComponent } from './life-entry-add/life-entry-add.component';
import { LifeEntryEditComponent } from './life-entry-edit/life-entry-edit.component';
import { LifeEntryActivityAddComponent } from './life-entry-activity-add/life-entry-activity-add.component';
import { LifeEntryActivityEditComponent } from './life-entry-activity-edit/life-entry-activity-edit.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    CalendarComponent,
    ActivityTypesComponent,
    ActivityTypeAddComponent,
    ActivityTypeEditComponent,
    ActivitiesComponent,
    ActivityAddComponent,
    ActivityEditComponent,
    DayEditComponent,
    LifeEntryAddComponent,
    LifeEntryEditComponent,
    LifeEntryActivityAddComponent,
    LifeEntryActivityEditComponent,
    SearchComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
