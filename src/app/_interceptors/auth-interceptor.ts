import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { AuthenticationService } from '../_services/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private auth: AuthenticationService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const currentUser = this.auth.currentUserValue;
        if (currentUser) {
            const authReq = req.clone({ setHeaders: { Authorization: `Basic ${this.auth.currentUserValue.authdata}` } });
            return next.handle(authReq);
        }
        return next.handle(req);
    }
}