import { Component, OnInit } from '@angular/core';
import { NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../_services/api.service';
import { Day } from '../_models/day';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  date: NgbDate;
  day: Day;

  constructor(
    private calendar: NgbCalendar,
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        let dateString = params['date'];
        if (dateString) {
          let parts = dateString.split('-', 3);
          this.date = new NgbDate(+parts[0], +parts[1], +parts[2]);
        } else {
          this.date = this.calendar.getToday();
        }

        this.loadDay(this.date);
      });
  }

  previousDay() {
    this.changeDate(this.calendar.getPrev(this.date));
  }

  nextDay() {
    this.changeDate(this.calendar.getNext(this.date));
  }

  onDateSelection(date: NgbDate) {
    this.changeDate(date);
  }

  changeDate(date: NgbDate) {
    this.router.navigate(['calendar'], { queryParams: { date: this.dateToString(date) } });
  }

  dateToString(date: NgbDate) {
    let year = date.year.toString();
    let month = this.get2DigitsNumberString(date.month);
    let day = this.get2DigitsNumberString(date.day);
    return `${year}-${month}-${day}`;
  }

  get2DigitsNumberString(value: number) {
    if (value < 10)
      return `0${value}`;
    else
      return value.toString();
  }

  loadDay(date: NgbDate) {
    this.api.getDay(this.dateToString(date)).subscribe(
      data => {
        if (!data) {
          this.day = null;
          return;
        }
        this.day = data;
        this.day.life_entries = this.day.life_entries.sort((a, b) => {
          if (a.start_time < b.start_time) { return -1; }
          if (a.start_time > b.start_time) { return 1; }
          return 0;
        });
      },
      error => {
        this.day = null;
      });
  }

  editDay(id: any) {
    this.router.navigate([`days/${id}`]);
  }

  newDay() {
    let day = new Day();
    day.date = this.dateToString(this.date);

    this.api.addDay(day).subscribe(
      data => {
        this.editDay(data.id);
      });
  }
}
