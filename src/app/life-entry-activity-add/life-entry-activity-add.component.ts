import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap, catchError } from 'rxjs/operators';

import { ApiService } from '../_services/api.service';
import { LifeEntryActivity } from '../_models/life-entry-activity';
import { Activity } from '../_models/activity';

@Component({
  selector: 'app-life-entry-activity-add',
  templateUrl: './life-entry-activity-add.component.html',
  styleUrls: ['./life-entry-activity-add.component.css']
})
export class LifeEntryActivityAddComponent implements OnInit {

  lifeEntryId: string;
  life_entry_activity_form: FormGroup;
  submitted = false;
  loading = false;
  searching = false;
  searchFailed = false;
  another = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.submitted = false;
    this.loading = false;
    this.searching = false;
    this.searchFailed = false;
    this.another = false;

    this.route.queryParams
      .subscribe(params => {
        this.lifeEntryId = params['lifeEntryId'];
      });
    this.life_entry_activity_form = this.formBuilder.group({
      activity: [null, Validators.required],
      description: [''],
      quantity: [1],
      rating: [0],
    });
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      tap(() => this.searching = true),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.api.searchActivities(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          })
        ),
      ),
      tap(() => this.searching = false),
    )

  formatter = (x: Activity) => x.name;

  get f() { return this.life_entry_activity_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.life_entry_activity_form.invalid) {
      return;
    }

    this.loading = true;

    let life_entry_activity = new LifeEntryActivity();
    life_entry_activity.life_entry_id = +this.lifeEntryId;
    life_entry_activity.activity_id = this.f.activity.value.id;
    life_entry_activity.description = this.f.description.value;
    life_entry_activity.quantity = this.f.quantity.value;
    life_entry_activity.rating = this.f.rating.value;

    this.api.addLifeEntryActivity(life_entry_activity)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        if (this.another)
          //this.router.navigate(['life_entry_activities/new'], { queryParams: { lifeEntryId: this.lifeEntryId } });
          this.ngOnInit();
        else
          this.back();
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  back() {
    this.router.navigate([`life_entries/${this.lifeEntryId}`]);
  }

}
