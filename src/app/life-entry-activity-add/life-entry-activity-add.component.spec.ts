import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeEntryActivityAddComponent } from './life-entry-activity-add.component';

describe('LifeEntryActivityAddComponent', () => {
  let component: LifeEntryActivityAddComponent;
  let fixture: ComponentFixture<LifeEntryActivityAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeEntryActivityAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeEntryActivityAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
