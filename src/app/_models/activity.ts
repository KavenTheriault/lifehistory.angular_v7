import { ActivityType } from "./activity-type";

export class Activity {
    id: number;
    user_id: number;
    created_date: Date;
    name: string;
    activity_type_id: number;
    activity_type : ActivityType;
}