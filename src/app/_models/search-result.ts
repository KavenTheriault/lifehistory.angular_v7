export class SearchResult {
    day_id: number;
    date: string;
    start_time: string;
    end_time: string;
    description: string;
    quantity: number;
    rating: number;
    activity_type_name: string;
    activity_name: string;
}