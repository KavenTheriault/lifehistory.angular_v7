export class SearchQuery {
    start_date: string;
    end_date: string;
    activity_type_id: number;
    activity_id: number;
    text: string;
}