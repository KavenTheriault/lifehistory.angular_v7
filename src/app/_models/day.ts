import { LifeEntry } from "./life-entry";

export class Day {
    id: number;
    user_id: number;
    created_date: Date;
    date: string;
    note: string;
    life_entries : LifeEntry[];
}