export class ActivityType {
    id: number;
    user_id: number;
    created_date: Date;
    name: string;
    show_quantity: boolean;
    show_rating: boolean;
}