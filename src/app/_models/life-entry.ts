import { LifeEntryActivity } from "./life-entry-activity";

export class LifeEntry {
    id: number;
    user_id: number;
    created_date: Date;
    day_id: number;
    start_time: string;
    end_time: string;
    life_entry_activities: LifeEntryActivity[];
}