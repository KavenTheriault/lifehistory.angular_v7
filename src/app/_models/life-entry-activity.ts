import { Activity } from "./activity";

export class LifeEntryActivity {
    id: number;
    user_id: number;
    created_date: Date;
    life_entry_id: number;
    activity_id: number;
    description: string;
    quantity: number;
    rating: number;
    activity: Activity;
}