import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ActivityTypesComponent } from './activity-types/activity-types.component';
import { ActivityTypeAddComponent } from './activity-type-add/activity-type-add.component';
import { ActivityTypeEditComponent } from './activity-type-edit/activity-type-edit.component';
import { ActivitiesComponent } from './activities/activities.component';
import { ActivityAddComponent } from './activity-add/activity-add.component';
import { ActivityEditComponent } from './activity-edit/activity-edit.component';
import { DayEditComponent } from './day-edit/day-edit.component';
import { LifeEntryAddComponent } from './life-entry-add/life-entry-add.component';
import { LifeEntryEditComponent } from './life-entry-edit/life-entry-edit.component';
import { LifeEntryActivityAddComponent } from './life-entry-activity-add/life-entry-activity-add.component';
import { LifeEntryActivityEditComponent } from './life-entry-activity-edit/life-entry-activity-edit.component';
import { SearchComponent } from './search/search.component';

import { AuthGuard } from './_guards/auth-guard';

const routes: Routes = [
  { path: '', redirectTo: '/signin', pathMatch: 'full' },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'calendar', component: CalendarComponent, canActivate: [AuthGuard] },
  { path: 'search', component: SearchComponent, canActivate: [AuthGuard] },
  { path: 'days/:id', component: DayEditComponent, canActivate: [AuthGuard] },
  { path: 'categories', component: ActivityTypesComponent, canActivate: [AuthGuard] },
  { path: 'categories/new', component: ActivityTypeAddComponent, canActivate: [AuthGuard] },
  { path: 'categories/:id', component: ActivityTypeEditComponent, canActivate: [AuthGuard] },
  { path: 'activities', component: ActivitiesComponent, canActivate: [AuthGuard] },
  { path: 'activities/new', component: ActivityAddComponent, canActivate: [AuthGuard] },
  { path: 'activities/:id', component: ActivityEditComponent, canActivate: [AuthGuard] },
  { path: 'life_entries/new', component: LifeEntryAddComponent, canActivate: [AuthGuard] },
  { path: 'life_entries/:id', component: LifeEntryEditComponent, canActivate: [AuthGuard] },
  { path: 'life_entry_activities/new', component: LifeEntryActivityAddComponent, canActivate: [AuthGuard] },
  { path: 'life_entry_activities/:id', component: LifeEntryActivityEditComponent, canActivate: [AuthGuard] },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
