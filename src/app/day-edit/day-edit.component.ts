import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ApiService } from '../_services/api.service';
import { LifeEntry } from '../_models/life-entry';

@Component({
  selector: 'app-day-edit',
  templateUrl: './day-edit.component.html',
  styleUrls: ['./day-edit.component.css']
})
export class DayEditComponent implements OnInit {

  day_form: FormGroup;
  day = null;
  submitted = false;
  loading = false;
  save_and_add = false;
  life_entry_to_delete: LifeEntry;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getDay(this.route.snapshot.params['id']);
    this.day_form = this.formBuilder.group({
      note: [null],
    });
  }

  getDay(id) {
    this.api.getDayWIthId(id).subscribe(data => {
      this.day = data;
      this.day.life_entries = this.day.life_entries.sort((a, b) => {
        if (a.start_time < b.start_time) { return -1; }
        if (a.start_time > b.start_time) { return 1; }
        return 0;
      });
      this.day_form.setValue({
        note: data.note,
      });
    });
  }

  get f() { return this.day_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.day_form.invalid) {
      return;
    }

    this.loading = true;
    this.day.note = this.f.note.value;

    this.api.updateDay(this.day.id, this.day)
      .subscribe(result => {
        this.loading = false;
        console.log(result);

        if (this.save_and_add)
          this.router.navigate(['life_entries/new'], { queryParams: { dayId: this.day.id } });
        else
          this.back();
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  setLifeEntryToDelete(life_entry: LifeEntry) {
    this.life_entry_to_delete = life_entry;
  }

  deleteLifeEntry() {
    this.loading = true;
    this.api.deleteLifeEntry(this.life_entry_to_delete.id)
      .subscribe(res => {
        this.loading = false;
        let index = this.day.life_entries.indexOf(this.life_entry_to_delete);
        this.day.life_entries.splice(index, 1);
      }, (err) => {
        console.log(err);
        this.loading = false;
      }
      );
  }

  editLifeEntry(id: any) {
    this.router.navigate([`life_entries/${id}`]);
  }

  back() {
    this.router.navigate(['calendar'], { queryParams: { date: this.day.date } });
  }

}
