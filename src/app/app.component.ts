import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './_services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Life History';
  year: number = new Date().getFullYear();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  isAuthenticated() {
    if (this.authenticationService.currentUserValue) {
      return true;
    }
    else {
      return false;
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/']);
  }
}

