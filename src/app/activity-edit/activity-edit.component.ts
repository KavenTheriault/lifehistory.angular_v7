import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap, catchError } from 'rxjs/operators';

import { ApiService } from '../_services/api.service';
import { ActivityType } from '../_models/activity-type';

@Component({
  selector: 'app-activity-edit',
  templateUrl: './activity-edit.component.html',
  styleUrls: ['./activity-edit.component.css']
})
export class ActivityEditComponent implements OnInit {

  activity_form: FormGroup;
  activity = null;
  submitted = false;
  loading = false;
  searching = false;
  searchFailed = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getActivity(this.route.snapshot.params['id']);
    this.activity_form = this.formBuilder.group({
      name: ['', Validators.required],
      activity_type: [null, Validators.required],
    });
  }

  getActivity(id) {
    this.api.getActivity(id).subscribe(data => {
      this.activity = data;
      this.activity_form.setValue({
        name: data.name,
        activity_type: data.activity_type,
      });
    });
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      tap(() => this.searching = true),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.api.searchActivityTypes(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          })
        ),
      ),
      tap(() => this.searching = false),
    )

  formatter = (x:ActivityType) => x.name;

  get f() { return this.activity_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.activity_form.invalid) {
      return;
    }

    this.loading = true;

    this.activity.name = this.f.name.value;
    this.activity.activity_type_id = this.f.activity_type.value.id;

    this.api.updateActivity(this.activity.id, this.activity)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        this.router.navigate(['activities']);
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  back() {
    this.router.navigate(['activities']);
  }

}
