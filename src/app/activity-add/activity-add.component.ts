import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap, catchError } from 'rxjs/operators';

import { ApiService } from '../_services/api.service';
import { Activity } from '../_models/activity';
import { ActivityType } from '../_models/activity-type';

@Component({
  selector: 'app-activity-add',
  templateUrl: './activity-add.component.html',
  styleUrls: ['./activity-add.component.css']
})
export class ActivityAddComponent implements OnInit {

  activity_form: FormGroup;
  submitted = false;
  loading = false;
  searching = false;
  searchFailed = false;

  constructor(
    private router: Router,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.activity_form = this.formBuilder.group({
      name: ['', Validators.required],
      activity_type: [null, Validators.required],
    });
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      tap(() => this.searching = true),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.api.searchActivityTypes(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          })
        ),
      ),
      tap(() => this.searching = false),
    )

  formatter = (x:ActivityType) => x.name;

  get f() { return this.activity_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.activity_form.invalid) {
      return;
    }

    this.loading = true;

    let activity = new Activity();
    activity.name = this.f.name.value;
    activity.activity_type_id = this.f.activity_type.value.id;

    this.api.addActivity(activity)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        this.router.navigate(['activities']);
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  back() {
    this.router.navigate(['activities']);
  }

}
