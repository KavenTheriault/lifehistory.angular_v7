import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ApiService } from '../_services/api.service';
import { ActivityType } from '../_models/activity-type';

@Component({
  selector: 'app-activity-type-add',
  templateUrl: './activity-type-add.component.html',
  styleUrls: ['./activity-type-add.component.css']
})
export class ActivityTypeAddComponent implements OnInit {

  activity_type_form: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private router: Router,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.activity_type_form = this.formBuilder.group({
      name: ['', Validators.required],
      show_quantity: [false, Validators.required],
      show_rating: [false, Validators.required],
    });
  }

  get f() { return this.activity_type_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.activity_type_form.invalid) {
      return;
    }

    this.loading = true;

    let activity_type = new ActivityType();
    activity_type.name = this.f.name.value;
    activity_type.show_quantity = this.f.show_quantity.value;
    activity_type.show_rating = this.f.show_rating.value;

    this.api.addActivityType(activity_type)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        this.back();
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  back() {
    this.router.navigate(['categories']);
  }

}
