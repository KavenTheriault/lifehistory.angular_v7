import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../_models/user';
import { AuthResult } from '../_models/auth-result';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    register(name: string, username: string, password: string) {
        return this.http.post(`${environment.api_url}/api/users`, { username, password, name });
    }

    login(username: string, password: string) {
        return this.http.post<AuthResult>(`${environment.api_url}/api/authenticate`, { username, password })
            .pipe(map(auth_result => {
                if (auth_result.authenticate_result) {
                    let user: User = new User(username, btoa(`${username}:${password}`))
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return auth_result;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}