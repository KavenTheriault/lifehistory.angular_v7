import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { ActivityType } from '../_models/activity-type';
import { Activity } from '../_models/activity';
import { environment } from 'src/environments/environment';
import { Day } from '../_models/day';
import { LifeEntry } from '../_models/life-entry';
import { LifeEntryActivity } from '../_models/life-entry-activity';
import { SearchResult } from '../_models/search-result';
import { SearchQuery } from '../_models/search-query';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const activity_types_url = `${environment.api_url}/api/activity_types`;
const activities_url = `${environment.api_url}/api/activities`;
const days_url = `${environment.api_url}/api/days`;
const life_entries_url = `${environment.api_url}/api/life_entries`;
const life_entry_activities_url = `${environment.api_url}/api/life_entry_activities`;

@Injectable({ providedIn: 'root' })
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  getActivityTypes(): Observable<ActivityType[]> {
    return this.http.get<ActivityType[]>(activity_types_url)
      .pipe(
        tap(_ => console.log('fetched ActivityTypes')),
        catchError(this.handleError('getActivityTypes', []))
      );
  }

  getActivityType(id: number): Observable<ActivityType> {
    return this.http.get<ActivityType>(`${activity_types_url}/${id}`).pipe(
      tap(_ => console.log(`fetched ActivityType id=${id}`)),
      catchError(this.handleError<ActivityType>(`getActivityType id=${id}`))
    );
  }

  addActivityType(ActivityType): Observable<ActivityType> {
    return this.http.post<ActivityType>(activity_types_url, ActivityType, httpOptions).pipe(
      tap((ActivityType: ActivityType) => console.log(`added ActivityType w/ id=${ActivityType.id}`)),
      catchError(this.handleError<ActivityType>('addActivityType'))
    );
  }

  updateActivityType(id, ActivityType): Observable<any> {
    return this.http.put(`${activity_types_url}/${id}`, ActivityType, httpOptions).pipe(
      tap(_ => console.log(`updated ActivityType id=${id}`)),
      catchError(this.handleError<any>('updateActivityType'))
    );
  }

  deleteActivityType(id): Observable<ActivityType> {
    return this.http.delete<ActivityType>(`${activity_types_url}/${id}`, httpOptions).pipe(
      tap(_ => console.log(`deleted ActivityType id=${id}`)),
      catchError(this.handleError<ActivityType>('deleteActivityType'))
    );
  }

  searchActivityTypes(term: string): Observable<ActivityType[]> {
    return this.http.get<ActivityType[]>(`${activity_types_url}/search/${term}`)
      .pipe(
        tap(_ => console.log('searched ActivityTypes')),
        catchError(this.handleError('searchActivityTypes', []))
      );
  }

  getActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(activities_url)
      .pipe(
        tap(_ => console.log('fetched Activities')),
        catchError(this.handleError('getActivities', []))
      );
  }

  getActivity(id: number): Observable<Activity> {
    return this.http.get<Activity>(`${activities_url}/${id}`).pipe(
      tap(_ => console.log(`fetched Activity id=${id}`)),
      catchError(this.handleError<Activity>(`getActivity id=${id}`))
    );
  }

  addActivity(Activity): Observable<Activity> {
    return this.http.post<Activity>(activities_url, Activity, httpOptions).pipe(
      tap((Activity: Activity) => console.log(`added Activity w/ id=${Activity.id}`)),
      catchError(this.handleError<Activity>('addActivity'))
    );
  }

  updateActivity(id, Activity): Observable<any> {
    return this.http.put(`${activities_url}/${id}`, Activity, httpOptions).pipe(
      tap(_ => console.log(`updated Activity id=${id}`)),
      catchError(this.handleError<any>('updateActivity'))
    );
  }

  deleteActivity(id): Observable<Activity> {
    return this.http.delete<Activity>(`${activities_url}/${id}`, httpOptions).pipe(
      tap(_ => console.log(`deleted Activity id=${id}`)),
      catchError(this.handleError<Activity>('deleteActivity'))
    );
  }

  searchActivities(term: string): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${activities_url}/search/${term}`)
      .pipe(
        tap(_ => console.log('searched Activities')),
        catchError(this.handleError('searchActivities', []))
      );
  }

  getDay(dateString: string): Observable<Day> {
    return this.http.get<Day>(`${days_url}/${dateString}`).pipe(
      tap(_ => console.log(`fetched Activity date=${dateString}`)),
      catchError(this.handleError<Day>(`getActivity date=${dateString}`))
    )
  }

  getDayWIthId(id: number): Observable<Day> {
    return this.http.get<Day>(`${days_url}/${id}`).pipe(
      tap(_ => console.log(`fetched Day id=${id}`)),
      catchError(this.handleError<Day>(`Day id=${id}`))
    );
  }

  addDay(Day): Observable<Day> {
    return this.http.post<Day>(days_url, Day, httpOptions).pipe(
      tap((Day: Day) => console.log(`added Day w/ id=${Day.id}`)),
      catchError(this.handleError<Day>('addDay'))
    );
  }

  updateDay(id, Day): Observable<any> {
    return this.http.put(`${days_url}/${id}`, Day, httpOptions).pipe(
      tap(_ => console.log(`updated Day id=${id}`)),
      catchError(this.handleError<any>('updateDay'))
    );
  }

  addLifeEntry(LifeEntry): Observable<LifeEntry> {
    return this.http.post<LifeEntry>(life_entries_url, LifeEntry, httpOptions).pipe(
      tap((LifeEntry: LifeEntry) => console.log(`added LifeEntry w/ id=${LifeEntry.id}`)),
      catchError(this.handleError<LifeEntry>('addLifeEntry'))
    );
  }

  getLifeEntry(id: number): Observable<LifeEntry> {
    return this.http.get<LifeEntry>(`${life_entries_url}/${id}`).pipe(
      tap(_ => console.log(`fetched LifeEntry id=${id}`)),
      catchError(this.handleError<LifeEntry>(`getLifeEntry id=${id}`))
    );
  }

  updateLifeEntry(id, LifeEntry): Observable<any> {
    return this.http.put(`${life_entries_url}/${id}`, LifeEntry, httpOptions).pipe(
      tap(_ => console.log(`updated LifeEntry id=${id}`)),
      catchError(this.handleError<any>('updateLifeEntry'))
    );
  }

  deleteLifeEntry(id): Observable<LifeEntry> {
    return this.http.delete<LifeEntry>(`${life_entries_url}/${id}`, httpOptions).pipe(
      tap(_ => console.log(`deleted LifeEntry id=${id}`)),
      catchError(this.handleError<LifeEntry>('deleteLifeEntry'))
    );
  }

  addLifeEntryActivity(LifeEntryActivity): Observable<LifeEntryActivity> {
    return this.http.post<LifeEntryActivity>(life_entry_activities_url, LifeEntryActivity, httpOptions).pipe(
      tap((LifeEntryActivity: LifeEntryActivity) => console.log(`added LifeEntryActivity w/ id=${LifeEntryActivity.id}`)),
      catchError(this.handleError<LifeEntryActivity>('addLifeEntryActivity'))
    );
  }

  getLifeEntryActivity(id: number): Observable<LifeEntryActivity> {
    return this.http.get<LifeEntryActivity>(`${life_entry_activities_url}/${id}`).pipe(
      tap(_ => console.log(`fetched LifeEntryActivity id=${id}`)),
      catchError(this.handleError<LifeEntryActivity>(`getLifeEntryActivity id=${id}`))
    );
  }

  updateLifeEntryActivity(id, LifeEntryActivity): Observable<any> {
    return this.http.put(`${life_entry_activities_url}/${id}`, LifeEntryActivity, httpOptions).pipe(
      tap(_ => console.log(`updated LifeEntryActivity id=${id}`)),
      catchError(this.handleError<any>('updateLifeEntryActivity'))
    );
  }

  deleteLifeEntryActivity(id): Observable<LifeEntryActivity> {
    return this.http.delete<LifeEntryActivity>(`${life_entry_activities_url}/${id}`, httpOptions).pipe(
      tap(_ => console.log(`deleted LifeEntryActivity id=${id}`)),
      catchError(this.handleError<LifeEntryActivity>('deleteLifeEntryActivity'))
    );
  }

  search(query: SearchQuery): Observable<SearchResult[]> {
    return this.http.post<SearchResult[]>(`${life_entries_url}/search`, query, httpOptions)
      .pipe(
        tap(_ => console.log('searched LifeEntries')),
        catchError(this.handleError('search', []))
      );
  }

}
