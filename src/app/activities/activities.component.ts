import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../_services/api.service';
import { Activity } from '../_models/activity';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {

  activities: Activity[] = [];
  activity_to_delete: Activity;
  loading = true;
  curPage : number = 1;
  pageSize : number = 10;

  constructor(
    private router: Router,
    private api: ApiService) { }

  ngOnInit() {
    this.api.getActivities()
      .subscribe(result => {
        this.activities = result.sort((a, b) => {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        });
        console.log(this.activities);
        this.loading = false;
      }, err => {
        console.log(err);
        this.loading = false;
      });
  }

  setActivityToDelete(activity: Activity) {
    this.activity_to_delete = activity;
  }

  deleteActivity() {
    this.loading = true;
    this.api.deleteActivity(this.activity_to_delete.id)
      .subscribe(res => {
        this.loading = false;
        let index = this.activities.indexOf(this.activity_to_delete);
        this.activities.splice(index, 1);
      }, (err) => {
        console.log(err);
        this.loading = false;
      }
      );
  }

  newActivity() {
    this.router.navigate(['activities/new']);
  }

  editActivity(id: any) {
    this.router.navigate([`activities/${id}`]);
  }

}
