import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap, catchError } from 'rxjs/operators';

import { ApiService } from '../_services/api.service';
import { Activity } from '../_models/activity';
import { ActivityType } from '../_models/activity-type';
import { SearchResult } from '../_models/search-result';
import { SearchQuery } from '../_models/search-query';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  search_form: FormGroup;
  loading = false;
  searchingAT = false;
  searchingA = false;
  searchFailedA = false;
  searchFailedAT = false;
  result: SearchResult[];
  curPage : number = 1;
  pageSize : number = 10;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.search_form = this.formBuilder.group({
      start: [null],
      end: [null],
      activity_type: [null],
      activity: [null],
      text: [''],
    });
  }

  formatter1 = (x: ActivityType) => x.name;
  formatter2 = (x: Activity) => x.name;

  searchActivityType = (text$: Observable<string>) =>
    text$.pipe(
      tap(() => this.searchingAT = true),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.api.searchActivityTypes(term).pipe(
          tap(() => this.searchFailedAT = false),
          catchError(() => {
            this.searchFailedAT = true;
            return of([]);
          })
        ),
      ),
      tap(() => this.searchingAT = false),
    )

  searchActivity = (text$: Observable<string>) =>
    text$.pipe(
      tap(() => this.searchingA = true),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.api.searchActivities(term).pipe(
          tap(() => this.searchFailedA = false),
          catchError(() => {
            this.searchFailedA = true;
            return of([]);
          })
        ),
      ),
      tap(() => this.searchingA = false),
    )

  get f() { return this.search_form.controls; }

  dateToString(date: NgbDate) {
    let year = date.year.toString();
    let month = this.get2DigitsNumberString(date.month);
    let day = this.get2DigitsNumberString(date.day);
    return `${year}-${month}-${day}`;
  }

  get2DigitsNumberString(value: number) {
    if (value < 10)
      return `0${value}`;
    else
      return value.toString();
  }

  onSubmit() {
    if (this.search_form.invalid) {
      return;
    }
    if (!this.f.start.value && !this.f.end.value && !this.f.activity_type.value && !this.f.activity.value && !this.f.text.value) {
      return;
    }

    this.loading = true;
    let query = new SearchQuery();

    query.start_date = this.f.start.value ? this.dateToString(this.f.start.value) : null;
    query.end_date = this.f.end.value ? this.dateToString(this.f.end.value) : null;
    query.activity_type_id = this.f.activity_type.value ? this.f.activity_type.value.id : null;
    query.activity_id = this.f.activity.value ? this.f.activity.value.id : null;
    query.text = this.f.text.value;

    this.api.search(query)
      .subscribe(data => {
        this.loading = false;
        console.log(data);
        this.result = data;
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  openDay(date: string) {
    this.router.navigate(['calendar'], { queryParams: { date: date } });
  }

}
