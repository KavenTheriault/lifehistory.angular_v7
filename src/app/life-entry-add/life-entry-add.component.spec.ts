import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeEntryAddComponent } from './life-entry-add.component';

describe('LifeEntryAddComponent', () => {
  let component: LifeEntryAddComponent;
  let fixture: ComponentFixture<LifeEntryAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeEntryAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeEntryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
