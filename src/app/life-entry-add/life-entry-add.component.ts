import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ApiService } from '../_services/api.service';
import { LifeEntry } from '../_models/life-entry';
import { NgbTime } from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';

@Component({
  selector: 'app-life-entry-add',
  templateUrl: './life-entry-add.component.html',
  styleUrls: ['./life-entry-add.component.css']
})
export class LifeEntryAddComponent implements OnInit {

  dayId: string;
  life_entry_form: FormGroup;
  submitted = false;
  loading = false;
  save_and_add = false;
  another = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.submitted = false;
    this.loading = false;
    this.save_and_add = false;
    this.another = false;

    this.route.queryParams
      .subscribe(params => {
        this.dayId = params['dayId'];
      });
    this.life_entry_form = this.formBuilder.group({
      start_time: [null, Validators.required],
      end_time: [null],
    });
  }

  get f() { return this.life_entry_form.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.life_entry_form.invalid) {
      return;
    }

    this.loading = true;

    let life_entry = new LifeEntry();
    life_entry.day_id = +this.dayId;
    life_entry.start_time = this.timeToString(this.f.start_time.value);
    if (this.f.end_time.value)
      life_entry.end_time = this.timeToString(this.f.end_time.value);

    this.api.addLifeEntry(life_entry)
      .subscribe(result => {
        this.loading = false;
        console.log(result);
        if (this.another)
          this.ngOnInit();
        else if (this.save_and_add)
          this.router.navigate(['life_entry_activities/new'], { queryParams: { lifeEntryId: result.id } });
        else
          this.back();
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
  }

  timeToString(time: NgbTime) {
    return `${time.hour}:${time.minute}:${time.second}`;
  }

  back() {
    this.router.navigate([`days/${this.dayId}`]);
  }

}
