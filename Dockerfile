FROM node:latest as builder

COPY . /app
WORKDIR /app

RUN npm install
RUN npm run build

FROM abiosoft/caddy:latest

COPY --from=builder /app/dist/lifehistory-ui/ /srv
ADD Caddyfile /etc/Caddyfile
