# LifehistoryUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## Development Requirement
```
npm install -g @angular/cli
npm install --save-dev @angular-devkit/build-angular
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

# Deployment
```
ng build --prod
scp -r dist/lifehistory-ui/ zoidqc@107.172.11.251:~
# replace the files in /var/www/life_html/
# sudo systemctl restart nginx
```